﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pear.Core
{
    /// <summary>
    /// 角色表
    /// </summary>
    public class Role : Entity<Guid>, IEntityTypeBuilder<Role>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public Role()
        {
            CreatedTime = DateTimeOffset.Now;
            IsDeleted = false;
            Enabled = true;
        }

        /// <summary>
        /// 角色名称
        /// </summary>
        [Required, MaxLength(32)]
        public string Name { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [MaxLength(200)]
        public string Remark { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 多对多
        /// </summary>
        public ICollection<User> Users { get; set; }

        /// <summary>
        /// 多对多中间表
        /// </summary>
        public List<UserRole> UserRoles { get; set; }

        /// <summary>
        /// 多对多
        /// </summary>
        public ICollection<Security> Securities { get; set; }

        /// <summary>
        /// 多对多中间表
        /// </summary>
        public List<RoleSecurity> RoleSecurities { get; set; }

        /// <summary>
        /// 配置多对多关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<Role> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasMany(p => p.Securities)
                .WithMany(p => p.Roles)
                .UsingEntity<RoleSecurity>(
                  u => u.HasOne(c => c.Security).WithMany(c => c.RoleSecurities).HasForeignKey(c => c.SecurityId)
                , u => u.HasOne(c => c.Role).WithMany(c => c.RoleSecurities).HasForeignKey(c => c.RoleId)
                , u =>
                {
                    u.HasKey(c => new { c.RoleId, c.SecurityId });
                    // 添加多对多种子数据
                    u.HasData(
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000001"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000002"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000003"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000004"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000005"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000006"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000007"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000008"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000009"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000010"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000011"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000012"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000013"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000014"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000015"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000016"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000017"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000018"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000019"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000020"), CreatedTime = DateTimeOffset.Now, IsDeleted = false },
                        new RoleSecurity { Id = Guid.NewGuid(), RoleId = new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"), SecurityId = new Guid("00000000-0000-0000-0000-000000000021"), CreatedTime = DateTimeOffset.Now, IsDeleted = false }
                    );
                });
        }
    }
}