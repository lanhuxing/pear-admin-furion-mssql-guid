﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 用户表种子数据 <see cref="Security"/>
    /// </summary>
    public class SecuritySeedData : IEntitySeedData<Security>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<Security> HasData(DbContext dbContext, Type dbContextLocator)
        {
            var list = new List<Security>();

            int index = 1;

            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.profile.self"    , Remark = "用户-我的描述", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.profile"         , Remark = "用户-描述查看", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.list"            , Remark = "用户-列表", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.modify.self"     , Remark = "用户-我的资料修改", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.modify"          , Remark = "用户-资料修改", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.delete"          , Remark = "用户-删除用户", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.add"             , Remark = "用户-添加用户", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.change.password" , Remark = "用户-修改密码", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.roles.self"      , Remark = "用户-我的角色", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.roles"           , Remark = "用户-角色", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.securities.self" , Remark = "用户-我的权限", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "user.service.securities"      , Remark = "用户-权限管理", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "role.service.list"            , Remark = "角色-列表", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "role.service.add"             , Remark = "角色-添加", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "role.service.modify"          , Remark = "角色-修改", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "role.service.delete"          , Remark = "角色-删除", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "role.service.give"            , Remark = "角色-配置", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "security.service.list"        , Remark = "权限-列表", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "security.service.give"        , Remark = "权限-配置", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "security.service.refresh"     , Remark = "权限-刷新", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });
            list.Add(new Security { Id = Guid.Parse((index++).ToString().PadLeft(32, '0')), UniqueName = "security.service.all"         , Remark = "权限-全部权限", CreatedTime = DateTimeOffset.Now, IsDeleted = false, Enabled = true, OrderNumber = index });

            return list;
        }
    }
}