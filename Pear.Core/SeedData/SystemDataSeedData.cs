﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 数据字典种子数据
    /// </summary>
    public class SystemDataSeedData : IEntitySeedData<SystemData>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SystemData> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<SystemData>
            {
                new SystemData { Id = new Guid("8875DCE4-D700-4609-9BEE-8BFFD541E800"),CreatedTime = DateTimeOffset.Now,IsDeleted=false,Name="男",Remark="男",Sequence=0,CategoryId=new Guid("676DFD09-40B3-4E0C-BE0C-857E5C59BC8D"),Enabled=true },
                new SystemData { Id = new Guid("83B07411-5F53-4038-8F4E-A7BA08144130"),CreatedTime =DateTimeOffset.Now,IsDeleted=false,Name="女",Remark="女",Sequence=1,CategoryId=new Guid("676DFD09-40B3-4E0C-BE0C-857E5C59BC8D"),Enabled=true },
            };
        }
    }
}