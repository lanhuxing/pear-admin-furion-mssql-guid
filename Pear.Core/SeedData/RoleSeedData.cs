﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 用户表种子数据 <see cref="Role"/>
    /// </summary>
    public class RoleSeedData : IEntitySeedData<Role>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<Role> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new Role
                {
                    Id=new Guid("CBA2EB98-2A55-40CB-BC8C-E815954E803E"),Name="超级管理员",Remark="拥有所有权限",CreatedTime=DateTimeOffset.Now,IsDeleted=false,Enabled=true
                },
                new Role
                {
                    Id=new Guid("F7220457-DBFE-4D73-A2AA-74D53E92CCA6"),Name="测试用户",Remark="只有测试权限",CreatedTime=DateTimeOffset.Now,IsDeleted=false,Enabled=true
                }
            };
        }
    }
}