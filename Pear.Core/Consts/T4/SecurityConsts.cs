﻿namespace Pear.Core
{
    /// <summary>
    /// 权限定义
    /// </summary>
    public static class SecurityConsts
    {
        /// <summary>
        /// 用户-我的描述
        /// </summary>
        public const string USER_SERVICE_PROFILE_SELF = "user.service.profile.self";
        /// <summary>
        /// 用户-描述查看
        /// </summary>
        public const string USER_SERVICE_PROFILE = "user.service.profile";
        /// <summary>
        /// 用户-列表
        /// </summary>
        public const string USER_SERVICE_LIST = "user.service.list";
        /// <summary>
        /// 用户-我的资料修改
        /// </summary>
        public const string USER_SERVICE_MODIFY_SELF = "user.service.modify.self";
        /// <summary>
        /// 用户-资料修改
        /// </summary>
        public const string USER_SERVICE_MODIFY = "user.service.modify";
        /// <summary>
        /// 用户-删除用户
        /// </summary>
        public const string USER_SERVICE_DELETE = "user.service.delete";
        /// <summary>
        /// 用户-添加用户
        /// </summary>
        public const string USER_SERVICE_ADD = "user.service.add";
        /// <summary>
        /// 用户-修改密码
        /// </summary>
        public const string USER_SERVICE_CHANGE_PASSWORD = "user.service.change.password";
        /// <summary>
        /// 用户-我的角色
        /// </summary>
        public const string USER_SERVICE_ROLES_SELF = "user.service.roles.self";
        /// <summary>
        /// 用户-角色
        /// </summary>
        public const string USER_SERVICE_ROLES = "user.service.roles";
        /// <summary>
        /// 用户-我的权限
        /// </summary>
        public const string USER_SERVICE_SECURITIES_SELF = "user.service.securities.self";
        /// <summary>
        /// 用户-权限管理
        /// </summary>
        public const string USER_SERVICE_SECURITIES = "user.service.securities";
        /// <summary>
        /// 角色-列表
        /// </summary>
        public const string ROLE_SERVICE_LIST = "role.service.list";
        /// <summary>
        /// 角色-添加
        /// </summary>
        public const string ROLE_SERVICE_ADD = "role.service.add";
        /// <summary>
        /// 角色-修改
        /// </summary>
        public const string ROLE_SERVICE_MODIFY = "role.service.modify";
        /// <summary>
        /// 角色-删除
        /// </summary>
        public const string ROLE_SERVICE_DELETE = "role.service.delete";
        /// <summary>
        /// 角色-配置
        /// </summary>
        public const string ROLE_SERVICE_GIVE = "role.service.give";
        /// <summary>
        /// 权限-列表
        /// </summary>
        public const string SECURITY_SERVICE_LIST = "security.service.list";
        /// <summary>
        /// 权限-配置
        /// </summary>
        public const string SECURITY_SERVICE_GIVE = "security.service.give";
        /// <summary>
        /// 权限-刷新
        /// </summary>
        public const string SECURITY_SERVICE_REFRESH = "security.service.refresh";
        /// <summary>
        /// 权限-全部权限
        /// </summary>
        public const string SECURITY_SERVICE_ALL = "security.service.all";

    }
}

  