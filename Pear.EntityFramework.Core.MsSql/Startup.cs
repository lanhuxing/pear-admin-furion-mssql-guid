﻿using Furion;
using Microsoft.Extensions.DependencyInjection;
using Pear.EntityFramework.Core.MsSql.DbContexts;
using System;

namespace Pear.EntityFramework.Core.MsSql
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<PearDbContext>();
            }, "Pear.Database.Migrations.MsSql");
        }
    }
}
