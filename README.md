# 移植版本
```
82defff 2021-01-06 13:35
```

# Pear Admin Furion

⚡  基于 .Net 5 平台 Furion 生态的落地实践


building ...  :boom: 

# To do something

- [x] 项目起草
- [x] 登录入口
- [x] 用户管理
- [x] 角色管理
- [x] 权限处理
- [x] 权限管理
- [x] 数据字典
- [ ] 基础设施
- [ ] 资源管理
- [ ] 配置中心
- [ ] 系统监控
- [ ] 定时任务
- [ ] 行为日志
 
# Preview address

[项目来源](https://gitee.com/pear-admin/pear-admin-furion)

[框架](https://gitee.com/monksoul/Furion)