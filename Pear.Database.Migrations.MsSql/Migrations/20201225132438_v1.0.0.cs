﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pear.Database.Migrations.MsSql.Migrations
{
    public partial class v100 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Security",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UniqueName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SystemDataCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: false),
                    HigherId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemDataCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemDataCategory_SystemDataCategory_HigherId",
                        column: x => x.HigherId,
                        principalTable: "SystemDataCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Account = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Nickname = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true),
                    Photo = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Synopsis = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    SigninedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoleSecurity",
                columns: table => new
                {
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SecurityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleSecurity", x => new { x.RoleId, x.SecurityId });
                    table.ForeignKey(
                        name: "FK_RoleSecurity_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleSecurity_Security_SecurityId",
                        column: x => x.SecurityId,
                        principalTable: "Security",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SystemData",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemData_SystemDataCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "SystemDataCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CreatedTime", "IsDeleted", "Name", "Remark", "UpdatedTime" },
                values: new object[,]
                {
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 28, DateTimeKind.Unspecified).AddTicks(2418), new TimeSpan(0, 8, 0, 0, 0)), false, "超级管理员", "拥有所有权限", null },
                    { new Guid("f7220457-dbfe-4d73-a2aa-74d53e92cca6"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 28, DateTimeKind.Unspecified).AddTicks(2435), new TimeSpan(0, 8, 0, 0, 0)), false, "测试用户", "只有测试权限", null }
                });

            migrationBuilder.InsertData(
                table: "Security",
                columns: new[] { "Id", "CreatedTime", "IsDeleted", "Remark", "UniqueName", "UpdatedTime" },
                values: new object[,]
                {
                    { new Guid("00000000-0000-0000-0000-000000000020"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5770), new TimeSpan(0, 8, 0, 0, 0)), false, "security.service.refresh", "security.service.refresh", null },
                    { new Guid("00000000-0000-0000-0000-000000000019"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5766), new TimeSpan(0, 8, 0, 0, 0)), false, "security.service.give", "security.service.give", null },
                    { new Guid("00000000-0000-0000-0000-000000000018"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5761), new TimeSpan(0, 8, 0, 0, 0)), false, "security.service.list", "security.service.list", null },
                    { new Guid("00000000-0000-0000-0000-000000000017"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5755), new TimeSpan(0, 8, 0, 0, 0)), false, "role.service.give", "role.service.give", null },
                    { new Guid("00000000-0000-0000-0000-000000000016"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5750), new TimeSpan(0, 8, 0, 0, 0)), false, "role.service.delete", "role.service.delete", null },
                    { new Guid("00000000-0000-0000-0000-000000000015"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5745), new TimeSpan(0, 8, 0, 0, 0)), false, "role.service.modify", "role.service.modify", null },
                    { new Guid("00000000-0000-0000-0000-000000000014"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5740), new TimeSpan(0, 8, 0, 0, 0)), false, "role.service.add", "role.service.add", null },
                    { new Guid("00000000-0000-0000-0000-000000000013"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5736), new TimeSpan(0, 8, 0, 0, 0)), false, "role.service.list", "role.service.list", null },
                    { new Guid("00000000-0000-0000-0000-000000000012"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5731), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.securities", "user.service.securities", null },
                    { new Guid("00000000-0000-0000-0000-000000000011"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5726), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.securities.self", "user.service.securities.self", null },
                    { new Guid("00000000-0000-0000-0000-000000000010"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5720), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.roles", "user.service.roles", null },
                    { new Guid("00000000-0000-0000-0000-000000000009"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5713), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.roles.self", "user.service.roles.self", null },
                    { new Guid("00000000-0000-0000-0000-000000000008"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5709), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.change.password", "user.service.change.password", null },
                    { new Guid("00000000-0000-0000-0000-000000000007"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5704), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.add", "user.service.add", null },
                    { new Guid("00000000-0000-0000-0000-000000000006"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5699), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.delete", "user.service.delete", null },
                    { new Guid("00000000-0000-0000-0000-000000000005"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5691), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.modify", "user.service.modify", null },
                    { new Guid("00000000-0000-0000-0000-000000000004"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5686), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.modify.self", "user.service.modify.self", null },
                    { new Guid("00000000-0000-0000-0000-000000000003"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5681), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.list", "user.service.list", null },
                    { new Guid("00000000-0000-0000-0000-000000000002"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5674), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.profile", "user.service.profile", null },
                    { new Guid("00000000-0000-0000-0000-000000000001"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 8, 0, 0, 0)), false, "user.service.profile.self", "user.service.profile.self", null }
                });

            migrationBuilder.InsertData(
                table: "SystemDataCategory",
                columns: new[] { "Id", "CreatedTime", "HigherId", "IsDeleted", "Name", "Remark", "Sequence", "UpdatedTime" },
                values: new object[] { new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 971, DateTimeKind.Unspecified).AddTicks(1829), new TimeSpan(0, 8, 0, 0, 0)), null, false, "性别", "性别", 0, null });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Account", "CreatedTime", "Gender", "IsDeleted", "Nickname", "Password", "Photo", "SigninedTime", "Synopsis", "UpdatedTime" },
                values: new object[] { new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc"), "admin", new DateTimeOffset(new DateTime(2020, 12, 17, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 8, 0, 0, 0)), 0, false, null, "21232f297a57a5a743894a0e4a801fc3", null, new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, null });

            migrationBuilder.InsertData(
                table: "RoleSecurity",
                columns: new[] { "RoleId", "SecurityId", "CreatedTime", "Id", "IsDeleted", "UpdatedTime" },
                values: new object[,]
                {
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000001"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6725), new TimeSpan(0, 8, 0, 0, 0)), new Guid("66832f42-c916-4f69-acbf-cb05835ebb0e"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000020"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6952), new TimeSpan(0, 8, 0, 0, 0)), new Guid("d2ef15b5-59f5-486a-a065-ca5bf800dc0c"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000019"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6947), new TimeSpan(0, 8, 0, 0, 0)), new Guid("0df95dd5-b0e1-4f49-97fc-16dca8214ce8"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000018"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6941), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2e7f51c9-25a2-4afc-9019-64817846c36b"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000017"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6937), new TimeSpan(0, 8, 0, 0, 0)), new Guid("a17c5af3-213f-446f-b525-0bd5136eb38f"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000016"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6929), new TimeSpan(0, 8, 0, 0, 0)), new Guid("37b5a3a3-f5cf-4366-84f1-19f6a4afc9f2"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000015"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6924), new TimeSpan(0, 8, 0, 0, 0)), new Guid("4bfb3fdf-39b8-4582-8adf-64d83a46c9e8"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000014"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6919), new TimeSpan(0, 8, 0, 0, 0)), new Guid("d845b1f0-2c42-40b4-9734-840a52e4567f"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000013"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6914), new TimeSpan(0, 8, 0, 0, 0)), new Guid("dce72ac0-86c3-4ae0-afa6-48782336eefa"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000011"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6903), new TimeSpan(0, 8, 0, 0, 0)), new Guid("6969c143-a0e8-408e-848f-64d84c77b4c7"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000012"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6909), new TimeSpan(0, 8, 0, 0, 0)), new Guid("46e767a4-a258-49a4-a932-2320c6a19f47"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000009"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6892), new TimeSpan(0, 8, 0, 0, 0)), new Guid("094bb038-082b-4d48-b22a-85c9631f1e98"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000008"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6881), new TimeSpan(0, 8, 0, 0, 0)), new Guid("49817cfe-419d-4dea-a1b2-9140662bf91c"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000007"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6875), new TimeSpan(0, 8, 0, 0, 0)), new Guid("e0100e91-7b8a-409f-9b6d-68c778ba6e6f"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000006"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6870), new TimeSpan(0, 8, 0, 0, 0)), new Guid("66ec703b-088f-461b-b2fd-8643481aac3b"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000005"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6864), new TimeSpan(0, 8, 0, 0, 0)), new Guid("1deea715-7e85-4879-a4dd-a81b7cbf0147"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000004"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6857), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ca7a26e9-3426-4398-b364-38f69cb9219b"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000003"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6793), new TimeSpan(0, 8, 0, 0, 0)), new Guid("71d91e5e-cdb1-4479-9fd2-8472a6a2cc6a"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000002"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6785), new TimeSpan(0, 8, 0, 0, 0)), new Guid("5b59f6c8-3ac3-4072-a064-b45a11eae560"), false, null },
                    { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000010"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6898), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2ae5dafa-598c-4cb4-806a-e83884e0de6e"), false, null }
                });

            migrationBuilder.InsertData(
                table: "SystemData",
                columns: new[] { "Id", "CategoryId", "CreatedTime", "IsDeleted", "Name", "Remark", "Sequence", "UpdatedTime" },
                values: new object[,]
                {
                    { new Guid("83b07411-5f53-4038-8f4e-a7ba08144130"), new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 962, DateTimeKind.Unspecified).AddTicks(7022), new TimeSpan(0, 8, 0, 0, 0)), false, "女", "女", 1, null },
                    { new Guid("8875dce4-d700-4609-9bee-8bffd541e800"), new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 962, DateTimeKind.Unspecified).AddTicks(5710), new TimeSpan(0, 8, 0, 0, 0)), false, "男", "男", 0, null }
                });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId", "CreatedTime", "Id", "IsDeleted", "UpdatedTime" },
                values: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc"), new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 32, DateTimeKind.Unspecified).AddTicks(4878), new TimeSpan(0, 8, 0, 0, 0)), new Guid("117f4508-d0df-4c56-a28b-e0d86f3bb195"), false, null });

            migrationBuilder.CreateIndex(
                name: "IX_RoleSecurity_SecurityId",
                table: "RoleSecurity",
                column: "SecurityId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemData_CategoryId",
                table: "SystemData",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemDataCategory_HigherId",
                table: "SystemDataCategory",
                column: "HigherId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId",
                table: "UserRole",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoleSecurity");

            migrationBuilder.DropTable(
                name: "SystemData");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "Security");

            migrationBuilder.DropTable(
                name: "SystemDataCategory");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
