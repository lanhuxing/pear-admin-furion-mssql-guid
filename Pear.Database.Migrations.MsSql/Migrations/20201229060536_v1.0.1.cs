﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pear.Database.Migrations.MsSql.Migrations
{
    public partial class v101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "User",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "SystemDataCategory",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "SystemData",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "Security",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "Role",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 812, DateTimeKind.Unspecified).AddTicks(555), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f7220457-dbfe-4d73-a2aa-74d53e92cca6"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 812, DateTimeKind.Unspecified).AddTicks(582), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000001") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4368), new TimeSpan(0, 8, 0, 0, 0)), new Guid("80e81916-f727-4926-944a-f982ff6cb2b9") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000002") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4424), new TimeSpan(0, 8, 0, 0, 0)), new Guid("4906b82c-36c8-47b0-8b1b-90d199761631") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000003") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4431), new TimeSpan(0, 8, 0, 0, 0)), new Guid("13a28bc5-96fe-4817-b327-db7e94f7fc89") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000004") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4437), new TimeSpan(0, 8, 0, 0, 0)), new Guid("92a3c54a-c75a-4d01-9f6d-b95de2400154") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000005") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4443), new TimeSpan(0, 8, 0, 0, 0)), new Guid("345f16c7-5379-4d2d-8bf2-33e8f7e111a5") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000006") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4448), new TimeSpan(0, 8, 0, 0, 0)), new Guid("732a8fec-4dad-4e31-8b31-be75acfe3d48") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000007") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4454), new TimeSpan(0, 8, 0, 0, 0)), new Guid("8f0d0561-e43b-417c-94be-6647c14bce77") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000008") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4459), new TimeSpan(0, 8, 0, 0, 0)), new Guid("f8b75a3f-2fea-4230-91e8-491cec5f7b51") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000009") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4469), new TimeSpan(0, 8, 0, 0, 0)), new Guid("eff6edeb-208d-4a71-b6fc-55c9c4495230") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000010") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4474), new TimeSpan(0, 8, 0, 0, 0)), new Guid("14ecc52f-6996-4d5b-a34e-3ab75efbb5f2") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000011") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4479), new TimeSpan(0, 8, 0, 0, 0)), new Guid("001acd89-3898-4b9e-8151-a21bd6f3e6b2") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000012") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4485), new TimeSpan(0, 8, 0, 0, 0)), new Guid("fe61004f-e476-49f6-b43c-efc11827d580") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000013") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4490), new TimeSpan(0, 8, 0, 0, 0)), new Guid("7399906a-ae22-4a60-8139-15f62802c0bb") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000014") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4495), new TimeSpan(0, 8, 0, 0, 0)), new Guid("fd816d19-7738-4e48-8c7a-c3d443900372") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000015") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4500), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ab16b569-4dbf-42eb-b35e-b96977a5eb5f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000016") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4505), new TimeSpan(0, 8, 0, 0, 0)), new Guid("6a7a98a5-de82-4d30-8bc4-b6f1526a13fe") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000017") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4513), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ed84cfcc-5885-4172-a733-757be746c61c") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000018") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4518), new TimeSpan(0, 8, 0, 0, 0)), new Guid("89cd3d48-ce33-4035-aee7-4e4024548f52") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000019") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4523), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2d22a249-93e0-43ba-a87d-a112da74dcba") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000020") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4528), new TimeSpan(0, 8, 0, 0, 0)), new Guid("c3b83711-25ee-4083-94e9-3db770c10b70") });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4677), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4719), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4726), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4732), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000005"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4738), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000006"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4746), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000007"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4751), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000008"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4756), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000009"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4761), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000010"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4768), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000011"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4773), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000012"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4778), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000013"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4783), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000014"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4788), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000015"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4793), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000016"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4798), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000017"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4803), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000018"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4809), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000019"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4814), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000020"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4819), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.InsertData(
                table: "Security",
                columns: new[] { "Id", "CreatedTime", "Enabled", "IsDeleted", "Remark", "UniqueName", "UpdatedTime" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000021"), new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4824), new TimeSpan(0, 8, 0, 0, 0)), true, false, "security.service.all", "security.service.all", null });

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("83b07411-5f53-4038-8f4e-a7ba08144130"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 746, DateTimeKind.Unspecified).AddTicks(6291), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("8875dce4-d700-4609-9bee-8bffd541e800"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 746, DateTimeKind.Unspecified).AddTicks(5030), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "SystemDataCategory",
                keyColumn: "Id",
                keyValue: new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"),
                columns: new[] { "CreatedTime", "Enabled" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 755, DateTimeKind.Unspecified).AddTicks(91), new TimeSpan(0, 8, 0, 0, 0)), true });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 816, DateTimeKind.Unspecified).AddTicks(3708), new TimeSpan(0, 8, 0, 0, 0)), new Guid("86b0a9c6-519d-452f-a548-ce21a58a167d") });

            migrationBuilder.InsertData(
                table: "RoleSecurity",
                columns: new[] { "RoleId", "SecurityId", "CreatedTime", "Id", "IsDeleted", "UpdatedTime" },
                values: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000021"), new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4533), new TimeSpan(0, 8, 0, 0, 0)), new Guid("626d97f5-7883-4dc8-ac54-cb633f6cd8d0"), false, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000021") });

            migrationBuilder.DeleteData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000021"));

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "SystemDataCategory");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "SystemData");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "Security");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "Role");

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 28, DateTimeKind.Unspecified).AddTicks(2418), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f7220457-dbfe-4d73-a2aa-74d53e92cca6"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 28, DateTimeKind.Unspecified).AddTicks(2435), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000001") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6725), new TimeSpan(0, 8, 0, 0, 0)), new Guid("66832f42-c916-4f69-acbf-cb05835ebb0e") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000002") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6785), new TimeSpan(0, 8, 0, 0, 0)), new Guid("5b59f6c8-3ac3-4072-a064-b45a11eae560") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000003") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6793), new TimeSpan(0, 8, 0, 0, 0)), new Guid("71d91e5e-cdb1-4479-9fd2-8472a6a2cc6a") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000004") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6857), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ca7a26e9-3426-4398-b364-38f69cb9219b") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000005") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6864), new TimeSpan(0, 8, 0, 0, 0)), new Guid("1deea715-7e85-4879-a4dd-a81b7cbf0147") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000006") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6870), new TimeSpan(0, 8, 0, 0, 0)), new Guid("66ec703b-088f-461b-b2fd-8643481aac3b") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000007") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6875), new TimeSpan(0, 8, 0, 0, 0)), new Guid("e0100e91-7b8a-409f-9b6d-68c778ba6e6f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000008") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6881), new TimeSpan(0, 8, 0, 0, 0)), new Guid("49817cfe-419d-4dea-a1b2-9140662bf91c") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000009") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6892), new TimeSpan(0, 8, 0, 0, 0)), new Guid("094bb038-082b-4d48-b22a-85c9631f1e98") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000010") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6898), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2ae5dafa-598c-4cb4-806a-e83884e0de6e") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000011") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6903), new TimeSpan(0, 8, 0, 0, 0)), new Guid("6969c143-a0e8-408e-848f-64d84c77b4c7") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000012") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6909), new TimeSpan(0, 8, 0, 0, 0)), new Guid("46e767a4-a258-49a4-a932-2320c6a19f47") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000013") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6914), new TimeSpan(0, 8, 0, 0, 0)), new Guid("dce72ac0-86c3-4ae0-afa6-48782336eefa") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000014") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6919), new TimeSpan(0, 8, 0, 0, 0)), new Guid("d845b1f0-2c42-40b4-9734-840a52e4567f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000015") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6924), new TimeSpan(0, 8, 0, 0, 0)), new Guid("4bfb3fdf-39b8-4582-8adf-64d83a46c9e8") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000016") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6929), new TimeSpan(0, 8, 0, 0, 0)), new Guid("37b5a3a3-f5cf-4366-84f1-19f6a4afc9f2") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000017") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6937), new TimeSpan(0, 8, 0, 0, 0)), new Guid("a17c5af3-213f-446f-b525-0bd5136eb38f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000018") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6941), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2e7f51c9-25a2-4afc-9019-64817846c36b") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000019") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6947), new TimeSpan(0, 8, 0, 0, 0)), new Guid("0df95dd5-b0e1-4f49-97fc-16dca8214ce8") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000020") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 27, DateTimeKind.Unspecified).AddTicks(6952), new TimeSpan(0, 8, 0, 0, 0)), new Guid("d2ef15b5-59f5-486a-a065-ca5bf800dc0c") });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5674), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5681), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5686), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000005"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5691), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000006"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5699), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000007"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5704), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000008"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5709), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000009"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5713), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000010"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5720), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000011"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5726), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000012"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5731), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000013"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5736), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000014"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5740), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000015"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5745), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000016"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5750), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000017"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5755), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000018"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5761), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000019"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5766), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000020"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 29, DateTimeKind.Unspecified).AddTicks(5770), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("83b07411-5f53-4038-8f4e-a7ba08144130"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 962, DateTimeKind.Unspecified).AddTicks(7022), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("8875dce4-d700-4609-9bee-8bffd541e800"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 962, DateTimeKind.Unspecified).AddTicks(5710), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemDataCategory",
                keyColumn: "Id",
                keyValue: new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 37, 971, DateTimeKind.Unspecified).AddTicks(1829), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 25, 21, 24, 38, 32, DateTimeKind.Unspecified).AddTicks(4878), new TimeSpan(0, 8, 0, 0, 0)), new Guid("117f4508-d0df-4c56-a28b-e0d86f3bb195") });
        }
    }
}
