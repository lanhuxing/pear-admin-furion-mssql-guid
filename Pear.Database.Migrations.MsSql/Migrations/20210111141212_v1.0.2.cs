﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pear.Database.Migrations.MsSql.Migrations
{
    public partial class v102 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderNumber",
                table: "Security",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(8386), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f7220457-dbfe-4d73-a2aa-74d53e92cca6"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(8405), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000001") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2393), new TimeSpan(0, 8, 0, 0, 0)), new Guid("c3df85d5-8d87-4d2f-9d93-d8139d03142a") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000002") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2449), new TimeSpan(0, 8, 0, 0, 0)), new Guid("04ad60c1-bd44-4d28-b64b-1109c7c3c002") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000003") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2457), new TimeSpan(0, 8, 0, 0, 0)), new Guid("88531cbd-c101-44c0-9a5f-e950ec3d03d9") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000004") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2509), new TimeSpan(0, 8, 0, 0, 0)), new Guid("768650dc-a2a2-4d04-a98c-42e73e50875e") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000005") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2515), new TimeSpan(0, 8, 0, 0, 0)), new Guid("417c48ca-c512-4dd1-811f-6137bd98a51d") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000006") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2521), new TimeSpan(0, 8, 0, 0, 0)), new Guid("1e2b9e6c-6d6f-4d44-aa88-25efaa43195b") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000007") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2526), new TimeSpan(0, 8, 0, 0, 0)), new Guid("e0c90019-7050-42c1-804e-73fdb11afb31") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000008") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2531), new TimeSpan(0, 8, 0, 0, 0)), new Guid("62a07188-94fe-43e7-8cb0-69bb5b07d28c") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000009") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2542), new TimeSpan(0, 8, 0, 0, 0)), new Guid("b37b6d95-6061-406f-b296-6d6b066c3e80") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000010") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2547), new TimeSpan(0, 8, 0, 0, 0)), new Guid("9923caa5-183c-45fb-b87b-d6b71c7a233f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000011") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2553), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2350c311-647f-4cbf-8539-88fcf84b135a") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000012") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2558), new TimeSpan(0, 8, 0, 0, 0)), new Guid("8aa1cc64-fba6-4715-8230-7a8e54e0ec79") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000013") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2563), new TimeSpan(0, 8, 0, 0, 0)), new Guid("34771491-09bf-4be3-9d71-bcb46092f5b6") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000014") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2568), new TimeSpan(0, 8, 0, 0, 0)), new Guid("92af65fc-3352-465f-9805-a69e96d2187f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000015") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2573), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2dcbe5c5-697b-4d43-b6e0-44d4bffec2fd") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000016") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2578), new TimeSpan(0, 8, 0, 0, 0)), new Guid("f2a5157f-72de-45a5-9e2a-7315b894388f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000017") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2586), new TimeSpan(0, 8, 0, 0, 0)), new Guid("15d8acf5-066a-45d2-8211-1f5f5f1c77a0") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000018") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2591), new TimeSpan(0, 8, 0, 0, 0)), new Guid("5426eb3c-cd36-4d57-91b0-588bea5cddcd") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000019") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2596), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ddf656f4-081a-4522-a6dc-b29a87be896a") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000020") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2601), new TimeSpan(0, 8, 0, 0, 0)), new Guid("f3ca17d4-7d73-4c66-be96-040afec58b54") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000021") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 287, DateTimeKind.Unspecified).AddTicks(2605), new TimeSpan(0, 8, 0, 0, 0)), new Guid("9c5aa275-6c98-4357-8f55-7749868f3216") });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1237), new TimeSpan(0, 8, 0, 0, 0)), 2, "用户-我的描述" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1639), new TimeSpan(0, 8, 0, 0, 0)), 3, "用户-描述查看" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1647), new TimeSpan(0, 8, 0, 0, 0)), 4, "用户-列表" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1654), new TimeSpan(0, 8, 0, 0, 0)), 5, "用户-我的资料修改" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000005"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1660), new TimeSpan(0, 8, 0, 0, 0)), 6, "用户-资料修改" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000006"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1670), new TimeSpan(0, 8, 0, 0, 0)), 7, "用户-删除用户" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000007"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1682), new TimeSpan(0, 8, 0, 0, 0)), 8, "用户-添加用户" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000008"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1689), new TimeSpan(0, 8, 0, 0, 0)), 9, "用户-修改密码" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000009"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1696), new TimeSpan(0, 8, 0, 0, 0)), 10, "用户-我的角色" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000010"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1704), new TimeSpan(0, 8, 0, 0, 0)), 11, "用户-角色" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000011"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1710), new TimeSpan(0, 8, 0, 0, 0)), 12, "用户-我的权限" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000012"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1717), new TimeSpan(0, 8, 0, 0, 0)), 13, "用户-权限管理" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000013"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1723), new TimeSpan(0, 8, 0, 0, 0)), 14, "角色-列表" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000014"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1729), new TimeSpan(0, 8, 0, 0, 0)), 15, "角色-添加" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000015"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1735), new TimeSpan(0, 8, 0, 0, 0)), 16, "角色-修改" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000016"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1772), new TimeSpan(0, 8, 0, 0, 0)), 17, "角色-删除" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000017"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1779), new TimeSpan(0, 8, 0, 0, 0)), 18, "角色-配置" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000018"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1786), new TimeSpan(0, 8, 0, 0, 0)), 19, "权限-列表" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000019"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1793), new TimeSpan(0, 8, 0, 0, 0)), 20, "权限-配置" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000020"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1799), new TimeSpan(0, 8, 0, 0, 0)), 21, "权限-刷新" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000021"),
                columns: new[] { "CreatedTime", "OrderNumber", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 290, DateTimeKind.Unspecified).AddTicks(1805), new TimeSpan(0, 8, 0, 0, 0)), 22, "权限-全部权限" });

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("83b07411-5f53-4038-8f4e-a7ba08144130"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 218, DateTimeKind.Unspecified).AddTicks(4356), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("8875dce4-d700-4609-9bee-8bffd541e800"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 218, DateTimeKind.Unspecified).AddTicks(3021), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemDataCategory",
                keyColumn: "Id",
                keyValue: new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 226, DateTimeKind.Unspecified).AddTicks(9343), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2021, 1, 11, 22, 12, 11, 293, DateTimeKind.Unspecified).AddTicks(2180), new TimeSpan(0, 8, 0, 0, 0)), new Guid("848e2077-a192-4f23-a1ca-0733beb71521") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderNumber",
                table: "Security");

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 812, DateTimeKind.Unspecified).AddTicks(555), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "Role",
                keyColumn: "Id",
                keyValue: new Guid("f7220457-dbfe-4d73-a2aa-74d53e92cca6"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 812, DateTimeKind.Unspecified).AddTicks(582), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000001") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4368), new TimeSpan(0, 8, 0, 0, 0)), new Guid("80e81916-f727-4926-944a-f982ff6cb2b9") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000002") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4424), new TimeSpan(0, 8, 0, 0, 0)), new Guid("4906b82c-36c8-47b0-8b1b-90d199761631") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000003") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4431), new TimeSpan(0, 8, 0, 0, 0)), new Guid("13a28bc5-96fe-4817-b327-db7e94f7fc89") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000004") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4437), new TimeSpan(0, 8, 0, 0, 0)), new Guid("92a3c54a-c75a-4d01-9f6d-b95de2400154") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000005") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4443), new TimeSpan(0, 8, 0, 0, 0)), new Guid("345f16c7-5379-4d2d-8bf2-33e8f7e111a5") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000006") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4448), new TimeSpan(0, 8, 0, 0, 0)), new Guid("732a8fec-4dad-4e31-8b31-be75acfe3d48") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000007") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4454), new TimeSpan(0, 8, 0, 0, 0)), new Guid("8f0d0561-e43b-417c-94be-6647c14bce77") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000008") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4459), new TimeSpan(0, 8, 0, 0, 0)), new Guid("f8b75a3f-2fea-4230-91e8-491cec5f7b51") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000009") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4469), new TimeSpan(0, 8, 0, 0, 0)), new Guid("eff6edeb-208d-4a71-b6fc-55c9c4495230") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000010") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4474), new TimeSpan(0, 8, 0, 0, 0)), new Guid("14ecc52f-6996-4d5b-a34e-3ab75efbb5f2") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000011") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4479), new TimeSpan(0, 8, 0, 0, 0)), new Guid("001acd89-3898-4b9e-8151-a21bd6f3e6b2") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000012") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4485), new TimeSpan(0, 8, 0, 0, 0)), new Guid("fe61004f-e476-49f6-b43c-efc11827d580") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000013") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4490), new TimeSpan(0, 8, 0, 0, 0)), new Guid("7399906a-ae22-4a60-8139-15f62802c0bb") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000014") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4495), new TimeSpan(0, 8, 0, 0, 0)), new Guid("fd816d19-7738-4e48-8c7a-c3d443900372") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000015") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4500), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ab16b569-4dbf-42eb-b35e-b96977a5eb5f") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000016") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4505), new TimeSpan(0, 8, 0, 0, 0)), new Guid("6a7a98a5-de82-4d30-8bc4-b6f1526a13fe") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000017") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4513), new TimeSpan(0, 8, 0, 0, 0)), new Guid("ed84cfcc-5885-4172-a733-757be746c61c") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000018") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4518), new TimeSpan(0, 8, 0, 0, 0)), new Guid("89cd3d48-ce33-4035-aee7-4e4024548f52") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000019") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4523), new TimeSpan(0, 8, 0, 0, 0)), new Guid("2d22a249-93e0-43ba-a87d-a112da74dcba") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000020") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4528), new TimeSpan(0, 8, 0, 0, 0)), new Guid("c3b83711-25ee-4083-94e9-3db770c10b70") });

            migrationBuilder.UpdateData(
                table: "RoleSecurity",
                keyColumns: new[] { "RoleId", "SecurityId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("00000000-0000-0000-0000-000000000021") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 811, DateTimeKind.Unspecified).AddTicks(4533), new TimeSpan(0, 8, 0, 0, 0)), new Guid("626d97f5-7883-4dc8-ac54-cb633f6cd8d0") });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000001"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4677), new TimeSpan(0, 8, 0, 0, 0)), "user.service.profile.self" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000002"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4719), new TimeSpan(0, 8, 0, 0, 0)), "user.service.profile" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000003"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4726), new TimeSpan(0, 8, 0, 0, 0)), "user.service.list" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000004"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4732), new TimeSpan(0, 8, 0, 0, 0)), "user.service.modify.self" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000005"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4738), new TimeSpan(0, 8, 0, 0, 0)), "user.service.modify" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000006"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4746), new TimeSpan(0, 8, 0, 0, 0)), "user.service.delete" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000007"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4751), new TimeSpan(0, 8, 0, 0, 0)), "user.service.add" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000008"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4756), new TimeSpan(0, 8, 0, 0, 0)), "user.service.change.password" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000009"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4761), new TimeSpan(0, 8, 0, 0, 0)), "user.service.roles.self" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000010"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4768), new TimeSpan(0, 8, 0, 0, 0)), "user.service.roles" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000011"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4773), new TimeSpan(0, 8, 0, 0, 0)), "user.service.securities.self" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000012"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4778), new TimeSpan(0, 8, 0, 0, 0)), "user.service.securities" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000013"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4783), new TimeSpan(0, 8, 0, 0, 0)), "role.service.list" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000014"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4788), new TimeSpan(0, 8, 0, 0, 0)), "role.service.add" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000015"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4793), new TimeSpan(0, 8, 0, 0, 0)), "role.service.modify" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000016"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4798), new TimeSpan(0, 8, 0, 0, 0)), "role.service.delete" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000017"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4803), new TimeSpan(0, 8, 0, 0, 0)), "role.service.give" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000018"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4809), new TimeSpan(0, 8, 0, 0, 0)), "security.service.list" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000019"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4814), new TimeSpan(0, 8, 0, 0, 0)), "security.service.give" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000020"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4819), new TimeSpan(0, 8, 0, 0, 0)), "security.service.refresh" });

            migrationBuilder.UpdateData(
                table: "Security",
                keyColumn: "Id",
                keyValue: new Guid("00000000-0000-0000-0000-000000000021"),
                columns: new[] { "CreatedTime", "Remark" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 813, DateTimeKind.Unspecified).AddTicks(4824), new TimeSpan(0, 8, 0, 0, 0)), "security.service.all" });

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("83b07411-5f53-4038-8f4e-a7ba08144130"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 746, DateTimeKind.Unspecified).AddTicks(6291), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemData",
                keyColumn: "Id",
                keyValue: new Guid("8875dce4-d700-4609-9bee-8bffd541e800"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 746, DateTimeKind.Unspecified).AddTicks(5030), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "SystemDataCategory",
                keyColumn: "Id",
                keyValue: new Guid("676dfd09-40b3-4e0c-be0c-857e5c59bc8d"),
                column: "CreatedTime",
                value: new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 755, DateTimeKind.Unspecified).AddTicks(91), new TimeSpan(0, 8, 0, 0, 0)));

            migrationBuilder.UpdateData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("cba2eb98-2a55-40cb-bc8c-e815954e803e"), new Guid("b8882b9d-515c-402b-b614-d9ccd088a5bc") },
                columns: new[] { "CreatedTime", "Id" },
                values: new object[] { new DateTimeOffset(new DateTime(2020, 12, 29, 14, 5, 35, 816, DateTimeKind.Unspecified).AddTicks(3708), new TimeSpan(0, 8, 0, 0, 0)), new Guid("86b0a9c6-519d-452f-a548-ce21a58a167d") });
        }
    }
}
