﻿using Pear.Extend.Page;
using System.ComponentModel.DataAnnotations;

namespace Pear.Application.UserCenter
{
    /// <summary>
    /// 获取权限列表参数
    /// </summary>
    public class GetSecurityListInput : ExtendPageModel
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; set; }
    }
}